FROM golang:latest

RUN mkdir /go-cli
WORKDIR /go-cli
COPY . .
RUN go build -o main .

CMD ["/go-cli/main"]
