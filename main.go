package main

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:   "go-cli",
	Short: "go Cli",
	Long:  "example cli",
	Run: func(cmd *cobra.Command, args []string) {
		println("Hello World!")
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		println(err)
		return
	}
}

func main() {
	Execute()
}
